# Springboot API

## Api de Cliente e Cidade onde é possível:
```
Cadastrar cidade
Cadastrar cliente
Consultar cidade pelo nome
Consultar cidade pelo estado
Consultar cliente pelo nome
Consultar cliente pelo Id
Remover cliente
Alterar o nome do cliente
```

## Installation
```
Clonar o projeto e importá-lo como projeto maven para a sua IDE favorita (eu usei o eclipse).
Subir servidor MYSQL e criar um banco de dados chamado "compasso".
As configurações de banco se encontram em application.properties no path compasso\src\main\resources\application.properties
Depois é só rodar a aplicação.
```


## Mapeamento dos end-points

```
Documentação completa em:
http://localhost:8080/swagger-ui.html#

```
