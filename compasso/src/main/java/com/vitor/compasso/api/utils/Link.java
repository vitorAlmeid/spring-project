package com.vitor.compasso.api.utils;

public final class Link {
	
	 public static final String CITY = "/api/cities";
	 public static final String CLIENT = "/api/clients";
	 
	 public static final String ID = "/{id}";
	 public static final String NAME = "/{name}";
	 public static final String NAME_OR_STATE = "?nameOrState=";
	
}
