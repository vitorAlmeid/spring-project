package com.vitor.compasso.api.dtos;

public class ClientNameDto {
	
	private String fullName;
	
	public ClientNameDto() {
	}
	
	public ClientNameDto(String fullName) {
		super();
		this.fullName = fullName;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
}
