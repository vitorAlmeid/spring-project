package com.vitor.compasso.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vitor.compasso.api.dtos.ClientDto;
import com.vitor.compasso.api.dtos.ClientNameDto;
import com.vitor.compasso.api.entities.Client;
import com.vitor.compasso.api.services.ClientService;
import com.vitor.compasso.api.utils.Link;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(Link.CLIENT)
@CrossOrigin(origins = "*")
@Api(value="Client API")
public class ClientController {
	
	private static final Logger log = LoggerFactory.getLogger(ClientController.class);
	
	@Autowired
	ClientService clientService;
	

	@GetMapping(Link.ID)
	@ApiOperation(value = "Retorna um único cliente por ID.")
	public Client findById(@PathVariable(value="id") long id) {
		log.info("Getting client by id");
		
		return clientService.findById(id);
	}
	
	@GetMapping
	@ApiOperation(value = "Retorna um ou mais clientes por NAME.")
	public List<Client> findByNameOrListAll(@RequestParam(value = "name", required = false) String name) {
		if (name == null) {
			log.info("Listing all clients");
			return clientService.listAll();
		} else {
			log.info("Finding clients by name");
			return clientService.findByName(name);
		}
	}
	
	@PostMapping
	@ApiOperation(value = "Registra um único cliente.")
	public Client register(@RequestBody ClientDto clientDto) throws NoSuchAlgorithmException, ParseException{
		log.info("Registring client");
		
		return clientService.registerClient(clientDto);
	}
	@PutMapping(Link.ID)
	@ApiOperation(value = "Atualiza o nome um único cliente.")
	public String update(@RequestBody ClientNameDto clientNameDto, @PathVariable(value="id") long id) throws NoSuchAlgorithmException, ParseException{
		log.info("Updating client");
		
		return clientService.updateClient(clientNameDto, id);
	}
	
	@DeleteMapping(Link.ID)
	@ApiOperation(value = "Deleta um único cliente por ID.")
	public String deleteById(@PathVariable(value="id") long id) {
		log.info("Deleting client");
		
		clientService.deleteById(id);
		return "user has been deleted";
	}

}
