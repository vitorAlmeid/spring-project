package com.vitor.compasso.api.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitor.compasso.api.dtos.CityDto;
import com.vitor.compasso.api.entities.City;
import com.vitor.compasso.api.repositories.CityRepository;

/**
 * @author vitor
 * 
 * Classe auxiliar da CityController.
 *
 */

@Service
public class CityService {
	
	private static final Logger log = LoggerFactory.getLogger(CityService.class);

	@Autowired
	CityRepository cityRepository;
	
	/**
	 * Método que lista todos as cidades.
	 */
	public List<City> listAll(){
		log.info("Listing all cities");
		
		return cityRepository.findAll();
	}
	
	
	/**
	 * Método que retorna uma única cidade.
	 * @param long id
	 */
	public City findCityById (long id) {
		log.info("Finding city by ID");
		
		return cityRepository.findById(id);
	}
	
	/**
	 * Método que retorna uma única cidade buscando por nome ou estado.
	 * @param String nameOrState
	 */
	public List<City> findCityByNameOrState (String query) {
		log.info("Finding cities by name or state");
		
		List<City> cities = new ArrayList<City>();
		if(query.length() == 2) {
			cities = (List<City>) cityRepository.findByState(query);
		}else {
			cities.add(cityRepository.findByName(query));
		}
		return cities;
	}

	/**
	 * Método que cadastra uma única cidade.
	 * @param CityDto
	 */
	public City registerCity(CityDto cityDto){
		log.info("Registring a new city");
		
		City city = new City(cityDto.getId(), cityDto.getName(), cityDto.getState());
		
		return cityRepository.save(city);
	}
}
