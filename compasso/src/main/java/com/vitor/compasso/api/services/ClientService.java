package com.vitor.compasso.api.services;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitor.compasso.api.dtos.ClientDto;
import com.vitor.compasso.api.dtos.ClientNameDto;
import com.vitor.compasso.api.entities.City;
import com.vitor.compasso.api.entities.Client;
import com.vitor.compasso.api.repositories.ClientRepository;

@Service
public class ClientService {

	private static final Logger log = LoggerFactory.getLogger(ClientService.class);

	@Autowired
	ClientRepository clientRepository;
	
	@Autowired
	CityService cityService;

	/**
	 * Método que lista todos os clientes.
	 */
	public List<Client> listAll() {
		log.info("Finding all clients");
		
		return clientRepository.findAll();
	}
	
	public Client findById(long id) {
		log.info("Finding client by ID");
		
		return clientRepository.findById(id);
	}
	
	public List<Client> findByName(String name) {
		log.info("Finding client by name");
		
		return clientRepository.findByFullName(name);
	}
	
	public void deleteById(long id) {
		log.info("Deleting client by id");
		
		clientRepository.deleteById(id);
	}

	public Client registerClient(ClientDto clientDto) throws ParseException {
		log.info("Registring a new client");
		
		List<City> cities = cityService.findCityByNameOrState(clientDto.getCity());  
		City city = new City();
		if (!cities.isEmpty()) {
			city = cities.get(0);
		}

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate birthDate = LocalDate.parse(clientDto.getDateOfBirth(), formatter);
		Date date = Date.from(birthDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		int age = calculateAge(birthDate, LocalDate.now());
		
		Client client = new Client(clientDto.getId(), clientDto.getFullName(), clientDto.getGender(), date, age, city);

		return clientRepository.save(client);

	}
	
	public String updateClient(ClientNameDto clientNameDto, long id) {
		String status;
		Client client = findById(id);
		if(!clientNameDto.getFullName().isEmpty()) {
			client.setFullName(clientNameDto.getFullName());	
			clientRepository.save(client);
			status = "Updated successfully";
		} else {
			status = "Updated failled";
		}
	
		return status;
	}

	
	private int calculateAge(LocalDate birthDate, LocalDate currentDate) {
		if ((birthDate != null) && (currentDate != null)) {
			return Period.between(birthDate, currentDate).getYears();
		} else {
			return 0;
		}
	}
}