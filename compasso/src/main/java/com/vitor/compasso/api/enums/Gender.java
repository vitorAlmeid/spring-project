package com.vitor.compasso.api.enums;

/**
 * @author vitor
 * 
 * Enum para representar o gênero do cliente.
 *
 */
public enum Gender {

	MALE,FEMALE
}
