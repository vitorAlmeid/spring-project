package com.vitor.compasso.api.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vitor.compasso.api.entities.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long>{
	
	City findById(long id);
	
	City findByName(String name);
	
	List<City> findByState(String state);
	
}
