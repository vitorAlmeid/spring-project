package com.vitor.compasso.api.dtos;

import org.hibernate.validator.constraints.Length;

public class CityDto {

	private Long id;
	
	
	@Length(min = 3, max = 30, message = "Name must have between 3 and 30 characters")
	private String name;
	
	@Length(min = 2, max = 2, message = "State can only have 2 characters")
	private String state;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	
}
