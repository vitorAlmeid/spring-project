package com.vitor.compasso.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vitor.compasso.api.dtos.CityDto;
import com.vitor.compasso.api.entities.City;
import com.vitor.compasso.api.services.CityService;
import com.vitor.compasso.api.utils.Link;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author vitor
 * 
 *         Classe que recebe as requisições http e realiza as chamadas de método
 *         para a classe de service.
 *
 */

@RestController
@RequestMapping(Link.CITY)
@CrossOrigin(origins = "*")
@Api(value = "City API")
public class CityController {

	private static final Logger log = LoggerFactory.getLogger(CityController.class);

	@Autowired
	CityService cityService;

	@GetMapping
	@ApiOperation(value = "Encontra uma ou mais cidades por nome da cidade ou estado. Se não passar parâmetro, lista todas.")
	public List<City> findCityByNameOrState(@RequestParam(value = "query", required = false) String query) {
		if (query == null) {
			log.info("Listing all cities");
			return cityService.listAll();
		} else {
			log.info("Finding city by name or state");
			return cityService.findCityByNameOrState(query);
		}
	}

	@PostMapping
	@ApiOperation(value = "Cadastra uma nova cidade.")
	public City register(@RequestBody CityDto cityDto) throws NoSuchAlgorithmException {
		log.info("Registring city");

		return cityService.registerCity(cityDto);
	}

	@GetMapping(Link.ID)
	@ApiOperation(value = "Encontra uma única cidade por ID.")
	public City findCityById(@PathVariable(value = "id") long id) {
		log.info("Finding city by ID");

		return cityService.findCityById(id);
	}
}
