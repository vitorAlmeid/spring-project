package com.vitor.compasso.api.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vitor.compasso.api.entities.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> { 
	

	public Client findById(long id);
	
	@Query(value = "SELECT * FROM client", nativeQuery = true)
	public List<Client> findAllClients();
	
	public List<Client> findByFullName(String fullName);
}
