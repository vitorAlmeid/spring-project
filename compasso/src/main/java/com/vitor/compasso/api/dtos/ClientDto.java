package com.vitor.compasso.api.dtos;


import com.vitor.compasso.api.enums.Gender;

public class ClientDto {

	private Long id;
	private String fullName;
	private Gender gender;
	private String dateOfBirth;
	private String city;
	
	
	public ClientDto() {
	}


	public ClientDto(Long id, String fullName, Gender gender, String dateOfBirth, String city) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.city = city;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
}
